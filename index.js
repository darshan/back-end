const express = require("express");
const app = express();
const osinfo = require("os");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/", (req, res, next) => {
  res.status(200).send(`Executing on ${osinfo.hostname()}`);
});

app.listen(8080, () => {
  console.log("Listening on port 8080");
});
